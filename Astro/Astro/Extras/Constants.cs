﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Media;

namespace Astro.Extras
{
    public class Constants
    {
        public static SolidColorBrush GetSolidColorBrush(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte r = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            SolidColorBrush myBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(255, r, g, b));
            return myBrush;
        }

        public static String sungradientDark = "#CA5200";
        public static String sunGradientLight = "#DB9B00";
        public static String moonGradientDark =  "#6B2084";
        public static String moonGradientLight =  "#AF4194";
        public static String calendarBorder = "#D77F33";
        public static String dawnText = "#4D5872";
        public static String darkSteel = "#2E3951";

    }
}
