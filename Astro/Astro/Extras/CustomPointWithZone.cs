﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Astro.Extras
{
    public class CustomPointWithZone
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int ZoneOffset { get; set; }
        public bool IsDaylightSaving { get; set; }
        public string DisplayName { get; set; }
        public string FullName { get; set; }

        public CustomPointWithZone()
        {

        }

        public CustomPointWithZone(double longitude, double latitude, int zoneoffset, bool isdaylightsaving, string displayname, string fullname)
        {
            Longitude = longitude;
            Latitude = latitude;
            ZoneOffset = zoneoffset;
            IsDaylightSaving = isdaylightsaving;
            DisplayName = displayname;
            FullName = fullname;
        }
    }
}
