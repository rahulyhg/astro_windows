﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Charting;
using Telerik.Core;
using Telerik.UI.Xaml.Controls.Chart;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Astro.Extras
{
    public class SunChartLabelStrategy : ChartSeriesLabelStrategy
    {
        public LabelStrategyOptions options = LabelStrategyOptions.DefaultVisual | LabelStrategyOptions.Measure;

        public override LabelStrategyOptions Options
        {
            get
            {
                return this.options;
            }
        }

        public override FrameworkElement CreateDefaultVisual(DataPoint point, int labelIndex)
        {
            ChartSeries series = point.Presenter as ChartSeries;
            return new Ellipse()
            {
                Stroke = new SolidColorBrush(Colors.Green)
            };
            //return base.CreateDefaultVisual(point, labelIndex);
        }

        public override RadSize GetLabelDesiredSize(DataPoint point, FrameworkElement visual, int labelIndex)
        {

            return new RadSize(10, 10);
           // return base.GetLabelDesiredSize(point, visual, labelIndex);
        }
    }
}
