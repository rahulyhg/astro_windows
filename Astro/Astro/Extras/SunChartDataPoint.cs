﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Astro.Extras
{
   public class SunChartDataPoint
    {
        public DateTime Time { get; set; }
        public double Value { get; set; }
    }
}
