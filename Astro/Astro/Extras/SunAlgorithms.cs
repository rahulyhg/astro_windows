﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Globalization;

namespace Astro.Extras
{
    public class SunAlgorithms
    {

        private static double calcTimeJulianCent(double jd)
        {
            double T = (jd - 2451545.0) / 36525.0;
            return T;
        }

        private static double radToDeg(double angleRad)
        {
            return (180.0 * angleRad / Math.PI);
        }

        private static double degToRad(double angleDeg)
        {
            return (Math.PI * angleDeg / 180.0);
        }

        private static double calcGeomMeanLongSun(double t)
        {
            double L0 = 280.46646 + t * (36000.76983 + t * (0.0003032));
            while (L0 > 360.0)
            {
                L0 -= 360.0;
            }
            while (L0 < 0.0)
            {
                L0 += 360.0;
            }
            return L0;        // in degrees
        }

        private static double calcGeomMeanAnomalySun(double t)
        {
            double M = 357.52911 + t * (35999.05029 - 0.0001537 * t);
            return M;        // in degrees
        }

        private static double calcEccentricityEarthOrbit(double t)
        {
            double e = 0.016708634 - t * (0.000042037 + 0.0000001267 * t);
            return e;        // unitless
        }

        private static double calcSunEqOfCenter(double t)
        {
            double m = calcGeomMeanAnomalySun(t);
            double mrad = degToRad(m);
            double sinm = Math.Sin(mrad);
            double sin2m = Math.Sin(mrad + mrad);
            double sin3m = Math.Sin(mrad + mrad + mrad);
            double C = sinm * (1.914602 - t * (0.004817 + 0.000014 * t)) + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289;
            return C;        // in degrees
        }

        private static double calcSunTrueLong(double t)
        {
            double l0 = calcGeomMeanLongSun(t);
            double c = calcSunEqOfCenter(t);
            double O = l0 + c;
            return O;        // in degrees
        }



        private static double calcSunApparentLong(double t)
        {
            double o = calcSunTrueLong(t);
            double omega = 125.04 - 1934.136 * t;
            double lambda = o - 0.00569 - 0.00478 * Math.Sin(degToRad(omega));
            return lambda;        // in degrees
        }

        private static double calcMeanObliquityOfEcliptic(double t)
        {
            double seconds = 21.448 - t * (46.8150 + t * (0.00059 - t * (0.001813)));
            double e0 = 23.0 + (26.0 + (seconds / 60.0)) / 60.0;
            return e0;        // in degrees
        }

        private static double calcObliquityCorrection(double t)
        {
            double e0 = calcMeanObliquityOfEcliptic(t);
            double omega = 125.04 - 1934.136 * t;
            double e = e0 + 0.00256 * Math.Cos(degToRad(omega));
            return e;        // in degrees
        }

        private static double calcSunDeclination(double t)
        {
            double e = calcObliquityCorrection(t);
            double lambda = calcSunApparentLong(t);

            double sint = Math.Sin(degToRad(e)) * Math.Sin(degToRad(lambda));
            double theta = radToDeg(Math.Asin(sint));
            return theta;        // in degrees
        }

        private static double calcEquationOfTime(double t)
        {
            double epsilon = calcObliquityCorrection(t);
            double l0 = calcGeomMeanLongSun(t);
            double e = calcEccentricityEarthOrbit(t);
            double m = calcGeomMeanAnomalySun(t);

            double y = Math.Tan(degToRad(epsilon) / 2.0);
            y *= y;

            double sin2l0 = Math.Sin(2.0 * degToRad(l0));
            double sinm = Math.Sin(degToRad(m));
            double cos2l0 = Math.Cos(2.0 * degToRad(l0));
            double sin4l0 = Math.Sin(4.0 * degToRad(l0));
            double sin2m = Math.Sin(2.0 * degToRad(m));

            double Etime = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0 - 0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m;
            double eTimeDeg = radToDeg(Etime) * 4.0;
            return eTimeDeg;    // in minutes of time
        }

        private static double calcHourAngleSunrise(double lat, double solarDec)
        {
            double latRad = degToRad(lat);
            double sdRad = degToRad(solarDec);
            double HAarg = (Math.Cos(degToRad(90.833)) / (Math.Cos(latRad) * Math.Cos(sdRad)) - Math.Tan(latRad) * Math.Tan(sdRad));
            double HA = Math.Acos(HAarg);
            return HA;        // in radians (for sunset, use -HA)
        }

        private static double getJD(DateTime selectedDate)
        {
            //create calendar instance to get date information
            Calendar calendar = new Calendar();
            calendar.SetDateTime(selectedDate);
            //get date information
            int docmonth = calendar.Month;
            int docday = calendar.Day;
            int docyear = calendar.Year;
            //get the julian date
            double A = Math.Floor(docyear / 100.0);
            double B = 2 - A + Math.Floor(A / 4);
            double JD = Math.Floor(365.25 * (docyear + 4716)) + Math.Floor(30.6001 * (docmonth + 1)) + docday + B - 1524.5;
            return JD;
        }

        private static DateTime minutesToDate(double minutes)
        {
            double floatHour = minutes / 60.0;
            double hour = Math.Floor(floatHour);
            double floatMinute = 60.0 * (floatHour - Math.Floor(floatHour));
            double minute = Math.Floor(floatMinute);
            double floatSec = 60.0 * (floatMinute - Math.Floor(floatMinute));
            double second = Math.Floor(floatSec + 0.5);
            if (second > 59)
            {
                second = 0;
                minute += 1;
            }
            if (minute > 59)
            {
                minute = 0;
                hour += 1;
            }
            //create a calendar with the times
            DateTime dateTime = new DateTime();
            var fixedDateTime = dateTime.Date.AddHours(hour).AddMinutes(minute).AddSeconds(second);
            return fixedDateTime;
        }


        private static double calcSunriseSetUTC(Boolean rise, double JD, double latitude, double longitude)
        {
            double t = calcTimeJulianCent(JD);
            double eqTime = calcEquationOfTime(t);
            double solarDec = calcSunDeclination(t);
            double hourAngle = calcHourAngleSunrise(latitude, solarDec);
            //alert("HA = " + radToDeg(hourAngle));
            if (!rise) hourAngle = -hourAngle;
            double delta = longitude + radToDeg(hourAngle);
            double timeUTC = 720 - (4.0 * delta) - eqTime;    // in minutes
            return timeUTC;
        }

        private static DateTime calcSunriseSet(Boolean rise, double JD, double latitude, double longitude, int timezone, Boolean dst)
        // rise = 1 for sunrise, 0 for sunset
        {
            //var id = ((rise) ? "risebox" : "setbox");
            double timeUTC = calcSunriseSetUTC(rise, JD, latitude, longitude);
            double newTimeUTC = calcSunriseSetUTC(rise, JD + timeUTC / 1440.0, latitude, longitude);

            double timeLocal = newTimeUTC + (timezone * 60.0);
            timeLocal += ((dst) ? 60.0 : 0.0);
            DateTime timeString;
            if (rise)
            {
                //this should print the time in the text box
                timeString = minutesToDate(timeLocal);
                //document.getElementById(id).value = timeString(timeLocal, 2);
            }
            else
            {
                double jday = JD;
                double increment = ((timeLocal < 0) ? 1 : -1);
                while ((timeLocal < 0.0) || (timeLocal >= 1440.0))
                {
                    timeLocal += increment * 1440.0;
                    jday -= increment;
                }
                //this should print the time in the text box
                timeString = minutesToDate(timeLocal);
                //document.getElementById(id).value = timeDateString(jday, timeLocal);
            }
            return timeString;
        }


        public static DateTime calculateSunrise(CustomPointWithZone customPoint, DateTime selectedDate)
        {
            //get the julian date
            double julianDate = getJD(selectedDate);
            //get the time zone as an integer
            int timeZone = customPoint.ZoneOffset;
            //int timeZone = selectedDate.getZone().toTimeZone().getRawOffset() / 1000 / 60 / 60;
            //check if the passed date is using DST
            Boolean dst = selectedDate.IsDaylightSavingTime();
            //get coordinates from location
            double lat = customPoint.Latitude;
            double lng = customPoint.Longitude;
            return calcSunriseSet(true, julianDate, lat, lng, timeZone, dst);
        }

        public static DateTime calculateSunset(CustomPointWithZone customPoint, DateTime selectedDate)
        {
            //get the julian date
            double julianDate = getJD(selectedDate);
            //get the time zone as an integer
            int timeZone = customPoint.ZoneOffset;
            //int timeZone = selectedDate.getZone().toTimeZone().getRawOffset() / 1000 / 60 / 60;
            //check if the passed date is using DST
            Boolean dst = selectedDate.IsDaylightSavingTime();
            //get coordinates from location
            double lat = customPoint.Latitude;
            double lng = customPoint.Longitude;
            return calcSunriseSet(false, julianDate, lat, lng, timeZone, dst);
        }

        public static DateTime calculateSolarNoon(CustomPointWithZone customPoint, DateTime selectedDate)
        {
            //get the julian date
            double julianDate = getJD(selectedDate);
            //get the time zone as an integer
            int timeZone = customPoint.ZoneOffset;
            //int timeZone = selectedDate.getZone().toTimeZone().getRawOffset() / 1000 / 60 / 60;
            //check if the passed date is using DST
            Boolean dst = selectedDate.IsDaylightSavingTime();
            //get coordinates from location
            double longitude = customPoint.Longitude;

            double tnoon = calcTimeJulianCent(julianDate - longitude / 360.0);
            double eqTime = calcEquationOfTime(tnoon);
            double solNoonOffset = 720.0 - (longitude * 4) - eqTime; // in minutes
            double newt = calcTimeJulianCent(julianDate + solNoonOffset / 1440.0);
            eqTime = calcEquationOfTime(newt);
            double solNoonLocal = 720 - (longitude * 4) - eqTime + (timeZone * 60.0);// in minutes
            if (dst) solNoonLocal += 60.0;
            while (solNoonLocal < 0.0)
            {
                solNoonLocal += 1440.0;
            }
            while (solNoonLocal >= 1440.0)
            {
                solNoonLocal -= 1440.0;
            }
            //set the noon box with the sol noon time
            return minutesToDate(solNoonLocal);
            //document.getElementById("noonbox").value = timeString(solNoonLocal, 3);
        }


    }
}
