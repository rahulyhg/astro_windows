﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Geolocation;
using System.Diagnostics;
using Windows.UI;
using Astro.Extras;
using System.Threading.Tasks;
using Telerik.UI.Xaml.Controls.Chart;

namespace Astro
{
    
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        LinearGradientBrush sunGradientBrush;
        LinearGradientBrush moonGradientBrush;
        SolidColorBrush grayBrush = new SolidColorBrush(Colors.DarkGray);
        SolidColorBrush whiteBrush = new SolidColorBrush(Colors.White);
        DateTime sunSelectedDate;

        public MainPage()
        {
            this.InitializeComponent();
            //setup the location array
            
            var zoneList = TimeZoneInfo.GetSystemTimeZones();
            foreach (var timeZone in zoneList)
            {
                Debug.WriteLine("the zone id = " + timeZone.Id);
            }
            //setup the date times when it's first launched
            sunSelectedDate = DateTime.Now;
            //setup the gradients used for the backgrounds of each pivot
            var sunDarkColor = Constants.GetSolidColorBrush(Constants.sungradientDark);
            var sunLightColor = Constants.GetSolidColorBrush(Constants.sunGradientLight);
            var sunGradientCollection = new GradientStopCollection
            {
                new GradientStop {Color = sunDarkColor.Color, Offset = 0 },
                new GradientStop {Color = sunLightColor.Color, Offset = 1 }
            };
            sunGradientBrush = new LinearGradientBrush(sunGradientCollection, 90);
            var moonDarkColor = Constants.GetSolidColorBrush(Constants.moonGradientDark);
            var moonLightColor = Constants.GetSolidColorBrush(Constants.moonGradientLight);
            var moonGradientCollection = new GradientStopCollection
            {
                new GradientStop {Color = moonDarkColor.Color, Offset = 0 },
                new GradientStop {Color = moonLightColor.Color, Offset = 1 }
            };
            moonGradientBrush = new LinearGradientBrush(moonGradientCollection, 90);
            //make sure the pivot control starts on the sun screen
            pivotControl.SelectedIndex = 1;
            //get the users location
            getLocation();
            
        }


        private async void getLocation()
        {
            //setup the geolocator 
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 10;
            geolocator.ReportInterval = 100;
            geolocator.MovementThreshold = 5;
            //get permission to access users location
            var status = await Geolocator.RequestAccessAsync();
            //if permission was granted, use geolocator to get location
            CustomPointWithZone customPoint = null;
            if (status == GeolocationAccessStatus.Allowed)
            {
                try
                {
                    Geoposition geoposition = await geolocator.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
                    //if the geo position is found use device time zone
                    var zoneInfo = TimeZoneInfo.Local;
                    customPoint = new CustomPointWithZone();
                    customPoint.Latitude = geoposition.Coordinate.Point.Position.Latitude;
                    customPoint.Longitude = geoposition.Coordinate.Point.Position.Longitude;
                    customPoint.ZoneOffset = zoneInfo.BaseUtcOffset.Hours;
                    customPoint.FullName = zoneInfo.StandardName;
                }
                catch (UnauthorizedAccessException ex)
                {
                    //some error means setup location a different way
                    Debug.WriteLine(ex.StackTrace);
                }
            
            }
            //use the location to setup the view
            setupDateTimesFromLocation(customPoint);
           
        }

        private void setupDateTimesFromLocation(CustomPointWithZone customPoint)
        {
            //call the calculators to get rise to set times using coordinate
            DateTime riseDateTime = SunAlgorithms.calculateSunrise(customPoint, sunSelectedDate);
            DateTime setDateTime = SunAlgorithms.calculateSunset(customPoint, sunSelectedDate);
            DateTime noonDateTime = SunAlgorithms.calculateSolarNoon(customPoint, sunSelectedDate);
            //get dawn and dusk times
            DateTime dawnDateTime = riseDateTime.AddMinutes(-30);
            DateTime duskDateTime = setDateTime.AddMinutes(30);
            //update chart horizontal axis to fit times
            DateTimeContinuousAxis dateAxis = (DateTimeContinuousAxis)sunChart.HorizontalAxis;
            dateAxis.Minimum = dawnDateTime.AddHours(-2);
            dateAxis.Maximum = duskDateTime.AddHours(2);
            //create the spline series for the chart view
            createRiseToSetSeries(riseDateTime, noonDateTime, setDateTime);
            createDawnToRiseSeries(dawnDateTime, riseDateTime);
            createSetToDuskSeries(setDateTime, duskDateTime);
        }

        private void createRiseToSetSeries(DateTime riseTime, DateTime noonTime, DateTime setTime)
        {
            //setup the chart data
            List<SunChartDataPoint> chartData = new List<SunChartDataPoint>();
            chartData.Add(new SunChartDataPoint() { Time = riseTime, Value = 0.5 });
            chartData.Add(new SunChartDataPoint() { Time = noonTime, Value = 2.0 });
            chartData.Add(new SunChartDataPoint() { Time = setTime, Value = 0.5 });
            //add the data to the series
            riseSetSeries.DataContext = chartData;
            //update color of the series
            riseSetSeries.Stroke = whiteBrush;
        }

        private void createDawnToRiseSeries(DateTime dawnTime, DateTime riseTime)
        {
            //setup the chart data 
            List<SunChartDataPoint> chartData = new List<SunChartDataPoint>();
            chartData.Add(new SunChartDataPoint() { Time = dawnTime, Value = 0 });
            chartData.Add(new SunChartDataPoint() { Time = riseTime, Value = 0.5 });
            //add the data to the series
            dawnRiseSeries.DataContext = chartData;
            //update colors of series
            SolidColorBrush dawnFillBrush = Constants.GetSolidColorBrush(Constants.dawnText);
            dawnFillBrush.Opacity = 0.5;
            dawnRiseSeries.Stroke = Constants.GetSolidColorBrush(Constants.dawnText);
            dawnRiseSeries.Fill = dawnFillBrush;
        }

        private void createSetToDuskSeries(DateTime setTime, DateTime duskTime)
        {
            //setup the chart data
            List<SunChartDataPoint> chartData = new List<SunChartDataPoint>();
            chartData.Add(new SunChartDataPoint() { Time = setTime, Value = 0.5 });
            chartData.Add(new SunChartDataPoint() { Time = duskTime, Value = 0 });
            //add the data to the series
            setDuskSeries.DataContext = chartData;
            //update the colors of the series 
            SolidColorBrush duskFillBrush = Constants.GetSolidColorBrush(Constants.dawnText);
            duskFillBrush.Opacity = 0.5;
            setDuskSeries.Stroke = Constants.GetSolidColorBrush(Constants.dawnText);
            setDuskSeries.Fill = duskFillBrush;
        }

        private void pivotControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //set all the headers to gray
            mapPivotHeader.Foreground = grayBrush;
            sunPivotHeader.Foreground = grayBrush;
            moonPivotHeader.Foreground = grayBrush;
            //get selected index after pivot
            int selectedIndex = pivotControl.SelectedIndex;
            //Map Screen
            if (selectedIndex == 0)
            {
                setupMapPivot();
                //update map header
                mapPivotHeader.Foreground = whiteBrush;
            }
            //Sun Screen
            else if (selectedIndex == 1)
            {
                setupSunPivot();
                //update the sun header
                sunPivotHeader.Foreground = whiteBrush;
            }
            //Moon Screen
            else if (selectedIndex == 2)
            {
                setupMoonPivot();
                //update the moon header
                moonPivotHeader.Foreground = whiteBrush;
            }
          
        }

        private void setupMapPivot()
        {
            //set the background color
            mainGrid.Background = Constants.GetSolidColorBrush(Constants.darkSteel);
        }

        private void setupSunPivot()
        {
            //set the background color
            mainGrid.Background = sunGradientBrush;
        }

        private void setupMoonPivot()
        {
            //set the background color
            mainGrid.Background = moonGradientBrush;
        }

        private void sunCalendar_SelectionChanged(object sender, Telerik.UI.Xaml.Controls.Input.Calendar.CurrentSelectionChangedEventArgs e)
        {
            //get the selected date from the sun calendar
            sunSelectedDate = e.NewSelection;
            Debug.WriteLine("the new selected date is : " + sunSelectedDate);
        }
    }
}
